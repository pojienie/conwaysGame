{-# lANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where
import ConwaysGame (Cell(..), initWorld, advance, reverseCell)
import Directional (Directional(..))
import Brick.Widgets.Center (hCenter, vCenter)
import qualified Graphics.Vty as V
import qualified Data.World as W
import qualified Brick as B

data AppData = AppData
    { scope :: Int
    , world :: W.World Cell
    }

instruction :: [B.Widget ()]
instruction =
    [ B.str "(left, right, up, down): move"
    , B.str "spacebar: flip current cell"
    , B.str "-/+: change scope"
    , B.str "enter: advance once"
    , B.str "ESC: quit"
    ]

appDraw :: AppData -> [B.Widget ()]
appDraw app = [vCenter $ B.vBox $ map hCenter $ worldWidget : instruction]
  where
    worldWidget = B.vBox $ map row $ reverse l
    decideColor (0, 0) = B.withAttr "focus"
    decideColor (x, y) =
        B.withAttr $ case W.get (x, y) w of
            Alive -> "alive"
            Dead -> "dead"
    l = let s = scope app in [-s..s]
    w = world app
    row y = B.hBox $ map (\x -> decideColor (x, y) $ B.str " ") l

appChooseCursor :: AppData
                -> [B.CursorLocation ()]
                -> Maybe (B.CursorLocation ())
appChooseCursor = const $ const Nothing

appHandleEvent :: AppData
               -> B.BrickEvent () ()
               -> B.EventM () (B.Next AppData)
appHandleEvent app (B.VtyEvent (V.EvKey key _))
    | key == V.KEsc = B.halt app
    | key == V.KEnter = B.continue $ app{world = advance w}
    | key == V.KLeft = B.continue $ app{world = left w}
    | key == V.KRight = B.continue $ app{world = right w}
    | key == V.KUp = B.continue $ app{world = up w}
    | key == V.KDown = B.continue $ app{world = down w}
    | key == V.KChar '+' = B.continue $ app{scope = min 9 (s + 1)}
    | key == V.KChar '-' = B.continue $ app{scope = max 2 (s - 1)}
    | key == V.KChar ' ' = B.continue $ app{world = W.set reverseCell (0, 0) w}
  where
    w = world app
    s = scope app
appHandleEvent app _ = B.continue app

appStartEvent :: AppData -> B.EventM () AppData
appStartEvent = pure . id

appAttrMap :: AppData -> B.AttrMap
appAttrMap = const $ B.attrMap V.defAttr [ ("focus", B.bg V.yellow)
                                         , ("alive", B.bg V.red)
                                         , ("dead", B.bg V.blue)
                                         ]

main :: IO ()
main =
    let app = B.App
                appDraw
                appChooseCursor
                appHandleEvent
                appStartEvent
                appAttrMap
        appData = AppData 2 initWorld
    in
    B.defaultMain app appData >> pure ()
