{- |
    This module define Directional class
-}
module Directional
    ( Directional(..)
    )where

-- | a directional class
class Directional a where
    -- | move left
    left :: a -> a
    -- | move right
    right :: a -> a
    -- | move up
    up :: a -> a
    -- | move down
    down :: a -> a
