{- |
    This module define Tape data type
-}
module Data.Tape
    ( Tape(..)
    , iterate
    , get
    , set
    )where
import Prelude hiding (iterate)
import Control.Comonad (Comonad(..))
import Directional (Directional(..))
import qualified Data.Stream as S

-- | tape data type, tape is an infinite list running both direction
data Tape a = Tape (S.Stream a) a (S.Stream a) deriving (Eq, Show)

instance Functor Tape where
    fmap f (Tape l v r) = Tape (fmap f l) (f v) (fmap f r)

instance Comonad Tape where
    extract (Tape _ v _) = v
    duplicate o = Tape (S.iterate left $ left o) o (S.iterate right $ right o)

instance Directional (Tape a) where
    left (Tape l v r) = Tape (S.tail l) (S.head l) (S.Stream v r)
    right (Tape l v r) = Tape (S.Stream v l) (S.head r) (S.tail r)
    up = right
    down = left

-- | iterate for tape
--   first argument determines how to generate left part of the test
--   second argument is the center of the tape and the seed of left and right
--   third argument determines how to generate right part of the test
iterate :: (a -> a) -> a -> (a -> a) -> Tape a
iterate l c r = Tape (S.iterate l $ l c) c (S.iterate r $ r c)

-- | get the element of given position
get :: Int -> Tape a -> a
get 0 = extract
get x
    | x > 0 = get (x - 1) . right
    | otherwise = get (x + 1) . left

-- | set a given position to a value
set :: (a -> a) -> Int -> Tape a -> Tape a
set f 0 (Tape l v r) = Tape l (f v) r
set f x (Tape l v r)
    | x > 0 = Tape l v (S.set f (x - 1) r)
    | otherwise = Tape (S.set f (- x - 1) l) v r
