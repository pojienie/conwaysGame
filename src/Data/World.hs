{- |
    This module define World data type
-}
module Data.World
    ( World(..)
    , emptyWorld
    , get
    , set
    )where
import Control.Comonad (Comonad(..))
import Directional (Directional(..))
import qualified Data.Tape as T

-- | conway's game world
data World a = World (T.Tape (T.Tape a)) deriving (Eq, Show)

instance Functor World where
    fmap f (World t) = World $ fmap (fmap f) t

instance Comonad World where
    extract (World t) = extract $ extract t
    duplicate o = World $ T.iterate (fmap left) (T.iterate down o up) (fmap right)

instance Directional (World a) where
    left (World t)  = World $ left t
    right (World t) = World $ right t
    up (World t) = World $ fmap up t
    down (World t) = World $ fmap down t

-- | empty world
emptyWorld :: a -> World a
emptyWorld v =
    let vTape = T.iterate (const v) v (const v)
    in World $ T.iterate (const vTape) vTape (const vTape)

-- | get the element of given position
get :: (Int, Int) -> World a -> a
get (0, 0) = extract
get (0, y)
    | y > 0 = get (0, y - 1) . up
    | otherwise = get (0, y + 1) . down
get (x, y)
    | x > 0 = get (x - 1, y) . right
    | otherwise = get (x + 1, y) . left

-- | get the element of given position
set :: (a -> a) -> (Int, Int) -> World a -> World a
set f (x, y) (World t) = World $ T.set (T.set f y) x t
