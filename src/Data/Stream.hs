{- |
    This module define Stream data type
-}
module Data.Stream
    ( Stream(..)
    , fromList
    , toList
    , iterate
    , head
    , tail
    , get
    , set
    )where
import Prelude hiding (head, tail, iterate)
import Control.Comonad (Comonad(..))
import Directional (Directional(..))

-- | stream data type, stream is a guaranteed infinite list
data Stream a = Stream a (Stream a)

instance Functor Stream where
    fmap f (Stream v r) = Stream (f v) $ fmap f r

instance Comonad Stream where
    extract (Stream v _) = v
    duplicate o@(Stream _ r) = Stream o $ duplicate r

-- | since it is impossible to show infinite list, we must limit it in some way
instance Show a => Show (Stream a) where
    show = show . take 100 . toList

-- | since it is impossible to test equality of infinite list
--   we must limit it in some way
instance Eq a => Eq (Stream a) where
    s1 == s2 = take 100 (toList s1) == take 100 (toList s2)

instance Directional (Stream a) where
    left (Stream _ r) = r
    right = left
    up = left
    down = left

-- | create a stream from a list
--   the first value is default value, used when the list has run out of element
fromList :: a -> [a] -> Stream a
fromList d (x:xs) = Stream x (fromList d xs)
fromList d [] = let s = Stream d s in s

-- | create a list from stream
toList :: Stream a -> [a]
toList (Stream v r) = v : toList r

-- | iterate for Stream
iterate :: (a -> a) -> a -> Stream a
iterate f v = Stream v $ iterate f $ f v

-- | head for stream
head :: Stream a -> a
head (Stream v _) = v

-- | tail for stream
tail :: Stream a -> Stream a
tail (Stream _ r) = r

-- | get the element of given position
get :: Int -> Stream a -> a
get 0 = extract
get x
    | x > 0 = get (x - 1) . right
    | otherwise = error "Stream.get is given negative value"

-- | set a given position to a value
set :: (a -> a) -> Int -> Stream a -> Stream a
set f 0 (Stream v r) = Stream (f v) r
set f x (Stream v r)
    | x > 0 = Stream v $ set f (x - 1) r
    | otherwise = error "Stream.set is given negative value"
