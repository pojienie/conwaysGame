{- |
    This module has all functions needed for Conways game
-}
module ConwaysGame
    ( Cell(..)
    , initWorld
    , advance
    , reverseCell
    ) where
import Control.Comonad (Comonad(extract, extend))
import Data.List (delete)
import qualified Data.World as W

-- | a cell in the game, it's either alive or dead
data Cell = Alive | Dead deriving (Eq)

-- | reverse the current status of a cell
reverseCell :: Cell -> Cell
reverseCell Alive = Dead
reverseCell Dead = Alive

-- | initial world
initWorld :: W.World Cell
initWorld = W.emptyWorld Dead

-- | rules on what cells are alive or dead in the next iteration
rule :: W.World Cell -> Cell
rule w =
    case length $ filter (== Alive) $ map (\p -> W.get p w) l of
        x | x > 3 -> Dead
          | x < 2 -> Dead
          | x == 3 -> Alive
          | otherwise -> extract w
  where
    l = delete (0, 0) $ do
        x <- [-1..1]
        y <- [-1..1]
        return (x, y)

-- | advance the game using rule
advance :: W.World Cell -> W.World Cell
advance = extend rule
