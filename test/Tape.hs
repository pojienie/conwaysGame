{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tape
    ( tapeTest
    )where
import Test.QuickCheck (quickCheck, Arbitrary(arbitrary))
import Test.QuickCheck.Function (Fun)
import Functor (functorLaw1, functorLaw2)
import Comonad (comonadLaw1, comonadLaw2, comonadLaw3)
import Stream ()
import qualified Data.Tape as T

instance Arbitrary a => Arbitrary (T.Tape a) where
    arbitrary = do
        l <- arbitrary
        v <- arbitrary
        r <- arbitrary
        return $ T.Tape l v r

tapeFunctorLaw1 :: T.Tape Int -> Bool
tapeFunctorLaw1 = functorLaw1

tapeFunctorLaw2 :: (Fun (Maybe Int) Float)
                  -> (Fun Int (Maybe Int))
                  -> T.Tape Int
                  -> Bool
tapeFunctorLaw2 = functorLaw2

tapeComonadLaw1 :: T.Tape Int -> Bool
tapeComonadLaw1 = comonadLaw1

tapeComonadLaw2 :: T.Tape Int -> Bool
tapeComonadLaw2 = comonadLaw2

tapeComonadLaw3 :: T.Tape Int -> Bool
tapeComonadLaw3 = comonadLaw3

tapeTest :: IO ()
tapeTest = do
    putStrLn "tape functor law 1"
    quickCheck tapeFunctorLaw1
    putStrLn "tape functor law 2"
    quickCheck tapeFunctorLaw2
    putStrLn "tape comonad law 1"
    quickCheck tapeComonadLaw1
    putStrLn "tape comonad law 2"
    quickCheck tapeComonadLaw2
    putStrLn "tape comonad law 3"
    quickCheck tapeComonadLaw3
