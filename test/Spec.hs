module Main where
import Stream (streamTest)
import Tape (tapeTest)
import World (worldTest)

main :: IO ()
main = do
    putStrLn "stream test"
    streamTest
    putStrLn "tape test"
    tapeTest
    putStrLn "world test"
    worldTest
