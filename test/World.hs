{-# OPTIONS_GHC -fno-warn-orphans #-}
module World
    ( worldTest
    )where
import Test.QuickCheck (quickCheck, Arbitrary(arbitrary))
import Test.QuickCheck.Function (Fun)
import Functor (functorLaw1, functorLaw2)
import Comonad (comonadLaw1, comonadLaw2, comonadLaw3)
import Tape ()
import qualified Data.World as W

instance Arbitrary a => Arbitrary (W.World a) where
    arbitrary = arbitrary >>= return . W.World

worldFunctorLaw1 :: W.World Int -> Bool
worldFunctorLaw1 = functorLaw1

worldFunctorLaw2 :: (Fun (Maybe Int) Float)
                  -> (Fun Int (Maybe Int))
                  -> W.World Int
                  -> Bool
worldFunctorLaw2 = functorLaw2

worldComonadLaw1 :: W.World Int -> Bool
worldComonadLaw1 = comonadLaw1

worldComonadLaw2 :: W.World Int -> Bool
worldComonadLaw2 = comonadLaw2

worldComonadLaw3 :: W.World Int -> Bool
worldComonadLaw3 = comonadLaw3

worldTest :: IO ()
worldTest = do
    putStrLn "world functor law 1"
    quickCheck worldFunctorLaw1
    putStrLn "world functor law 2"
    quickCheck worldFunctorLaw2
    putStrLn "world comonad law 1"
    quickCheck worldComonadLaw1
    putStrLn "world comonad law 2"
    quickCheck worldComonadLaw2
    putStrLn "world comonad law 3"
    quickCheck worldComonadLaw3
