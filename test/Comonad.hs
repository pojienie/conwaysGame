module Comonad
    ( comonadLaw1
    , comonadLaw2
    , comonadLaw3
    ) where

import Control.Comonad (Comonad(..))

-- | comonad law:
--     Law 1: extract . duplicate      = id
--     Law 2: fmap extract . duplicate = id
--     Law 3: duplicate . duplicate    = fmap duplicate . duplicate
comonadLaw1 :: (Comonad c, Eq (c a)) => c a -> Bool
comonadLaw1 = (\s -> (extract . duplicate) s == id s)

comonadLaw2 :: (Comonad c, Eq (c a)) => c a -> Bool
comonadLaw2 = (\s -> (fmap extract . duplicate) s == id s)

comonadLaw3 :: (Comonad c, Eq (c (c (c a)))) => c a -> Bool
comonadLaw3 = (\s -> (duplicate . duplicate) s == (fmap duplicate . duplicate) s)
