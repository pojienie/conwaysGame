module Functor
    ( functorLaw1
    , functorLaw2
    ) where
import Test.QuickCheck.Function (Fun, apply)

-- | functor law:
--     Law 1: fmap id = id
--     Law 2: fmap (g . h) = (fmap g) . (fmap h)
functorLaw1 :: (Functor f, Eq (f a)) => f a -> Bool
functorLaw1 = (\s -> fmap id s == id s)

functorLaw2 :: (Functor f, Eq (f c)) => (Fun b c) -> (Fun a b) -> f a -> Bool
functorLaw2 g' h'
    = let g = apply g'
          h = apply h' in
    (\s -> fmap (g . h) s == ((fmap g) . (fmap h)) s)
