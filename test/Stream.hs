{-# OPTIONS_GHC -fno-warn-orphans #-}
module Stream
    ( streamTest
    )where
import Test.QuickCheck (quickCheck, Arbitrary(arbitrary))
import Test.QuickCheck.Function (Fun)
import Functor (functorLaw1, functorLaw2)
import Comonad (comonadLaw1, comonadLaw2, comonadLaw3)
import qualified Data.Stream as S

instance Arbitrary a => Arbitrary (S.Stream a) where
    arbitrary = do
        d <- arbitrary
        l <- arbitrary
        return $ S.fromList d l

streamFunctorLaw1 :: S.Stream Int -> Bool
streamFunctorLaw1 = functorLaw1

streamFunctorLaw2 :: (Fun (Maybe Int) Float)
                  -> (Fun Int (Maybe Int))
                  -> S.Stream Int
                  -> Bool
streamFunctorLaw2 = functorLaw2

streamComonadLaw1 :: S.Stream Int -> Bool
streamComonadLaw1 = comonadLaw1

streamComonadLaw2 :: S.Stream Int -> Bool
streamComonadLaw2 = comonadLaw2

streamComonadLaw3 :: S.Stream Int -> Bool
streamComonadLaw3 = comonadLaw3

streamTest :: IO ()
streamTest = do
    putStrLn "stream functor law 1"
    quickCheck streamFunctorLaw1
    putStrLn "stream functor law 2"
    quickCheck streamFunctorLaw2
    putStrLn "stream comonad law 1"
    quickCheck streamComonadLaw1
    putStrLn "stream comonad law 2"
    quickCheck streamComonadLaw2
    putStrLn "stream comonad law 3"
    quickCheck streamComonadLaw3
